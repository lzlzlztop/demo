from django.shortcuts import render, redirect, reverse
from django.views.generic.base import TemplateView
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate

# Create your views here.



def loginView(request):
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        if User.objects.filter(username=username):
            # 验证账号密码与模型User的账号密码是否一致
            user = authenticate(username=username, password=password)
            # 通过验证则使用内置函数login执行用户登录
            # 登录成功后跳转到个人中心页
            if user:
                login(request, user)
                return redirect(reverse('blogs:blogs'))
        else:
            # 执行用户注册
            state = '注册成功'
            d = dict(username=username, password=password, is_staff=1, is_active=1)
            user = User.objects.create_user(**d)
            user.save()
    return render(request, 'login.html', locals())
