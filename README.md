Django
深入入门方法：
Pythoncharm中创建django项目
需求分析：分为两个应用一个为登录，一个为内容页
分别创建应用
python3 manage.py startapp bolgs
python3 manage.py startapp login
不支持在 Docs 外粘贴 block
运行可成功运行django服务
在settings中添加刚创建的应用
T模板：用于统一管理各个应用的模板文件
本地安装启用MySQL服务
pip安装pymysql模块
settings中配置数据库信息
项目的init中添加数据库链接模块
import pymysql
pymysql.version_info = (1, 4, 13, "final", 0)
pymysql.install_as_MySQLdb()

数据库迁移django内置功能表 python3 manage.py migrate
配置静态资源
STATIC_URL：默认配置时，用于识别项目应用文件夹里的static文件夹静态资源
STATICFILES_DIRS：额外添加静态文件的目录
STATIC_ROOT：生产环境时的静态文件目录
设置路由分发规则
每个应用中分别创建路由文件，路由分发给应用，应用单独路由
path方法设置路由
render来将数据渲染到模板中